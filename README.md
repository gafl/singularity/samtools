# samtools Singularity container
### Bionformatics package samtools<br>
Tools for dealing with SAM, BAM and CRAM files<br>
samtools Version: 1.10<br>
[https://github.com/samtools/samtools]

Singularity container based on the recipe: Singularity.samtools_v1.10

Package installation using Miniconda3-4.7.12<br>

Image singularity (V>=3.3) is automatically build and deployed (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

### build:
`sudo singularity build samtools_v1.10.sif Singularity.samtools_v1.10`

### Get image help
`singularity run-help ./samtools_v1.10.sif`

#### Default runscript: STAR
#### Usage:
  `samtools_v1.10.sif --help`<br>
    or:<br>
  `singularity exec samtools_v1.10.sif samtools --help`<br>


### Get image (singularity version >=3.3) with ORAS:<br>
`singularity pull samtools_v1.10.sif oras://registry.forgemia.inra.fr/gafl/singularity/samtools/samtools:latest`


